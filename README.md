# [symfony](https://phppackages.org/s/symfony)/[css-selector](https://phppackages.org/p/symfony/css-selector)

The CssSelector component converts CSS selectors to XPath expressions. https://symfony.com/css-selector

Unofficial howto and demo

## In other languages than PHP
### Python
* [cssselect](https://pypi.org/project/cssselect/)